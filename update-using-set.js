db.tasks.update(
    {
        description: 'Prepare for next module.'
    },
    {
        $set:
        {
            isCompleted: true
        }
    }
)