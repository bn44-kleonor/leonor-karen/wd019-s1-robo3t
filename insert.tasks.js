db.tasks.insertMany([
    {
        description: "lorem ipsum 1",
        teamId: "5da8435abf41ca1329b57d28",
        isCompleted: true,
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        description: "lorem ipsum 2",
        teamId: "5da8435abf41ca1329b57d28",
        isCompleted: false,
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        description: "lorem ipsum 3",
        teamId: "5da8435abf41ca1329b57d28",
        isCompleted: true,
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        description: "lorem ipsum 4",
        teamId: "5da8435abf41ca1329b57d28",
        isCompleted: false,
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        description: "lorem ipsum 5",
        teamId: "5da8435abf41ca1329b57d28",
        isCompleted: true,
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        description: "lorem ipsum 6",
        teamId: "5da8435abf41ca1329b57d28",
        isCompleted: false,
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        description: "lorem ipsum 7",
        teamId: "5da8435abf41ca1329b57d28",
        isCompleted: true,
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        description: "lorem ipsum 8",
        teamId: "5da8435abf41ca1329b57d28",
        isCompleted: false,
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        description: "lorem ipsum 9",
        teamId: "5da8435abf41ca1329b57d28",
        isCompleted: true,
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        description: "lorem ipsum 10",
        teamId: "5da8435abf41ca1329b57d28",
        isCompleted: false,
        createdAt: new Date(),
        updatedAt: new Date()
    }
])
