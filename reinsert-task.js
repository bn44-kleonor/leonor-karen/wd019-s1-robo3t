db.tasks.insert(
    {
        description: 'Prepare for next module.' ,
        teamId: ObjectId("5da8435abf41ca1329b57d28"),
        isCompleted: true,
        createdAt: new Date(),
        updatedAt: new Date()
    }
)